# Keycloak scripts
A collection of scripts to manage Keycloak.

Scripts to manage the realms/clients makes use of the
[Admin CLI](https://github.com/keycloak/keycloak-documentation/blob/3.4.3.Final/server_admin/topics/admin-cli.adoc).
Peruse the docs for a general idea of how it's used.

TL;DR: add `kcadm.sh` to your path. Then execute the script.
