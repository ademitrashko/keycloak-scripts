#!/usr/bin/env bash

command -v kcadm.sh >/dev/null 2>&1 || {
  echo >&2 "error: kcadm.sh command not found"
  exit 1
}

printf 'Keycloak realm: '
read -r realm

printf 'Keycloak URL (http://localhost:8080/auth): '
read -r server
server="${server:-http://localhost:8080/auth}"

printf 'LDAP connection URL: '
read -r ldapConnectionUrl

printf 'LDAP connection password: '
read -r -s ldapConnectionPassword

printf '\n'

printf 'Keycloak username: '
read -r username

printf 'Keycloak password: '
read -r -s password

printf '\n'

kcadm.sh config credentials --server "${server}" --realm "${realm}" --user "${username}" --password "${password}"

kcadm.sh create components \
-r "${realm}"  \
-s name="Commerce ITS LDAP" \
-s providerId=ldap  \
-s providerType=org.keycloak.storage.UserStorageProvider \
-s parentId="${realm}" \
-s 'config.enabled=["true"]' \
-s 'config.priority=["0"]' \
-s 'config.fullSyncPeriod=["-1"]' \
-s 'config.changedSyncPeriod=["-1"]' \
-s 'config.cachePolicy=["DEFAULT"]' \
-s config.evictionDay=[] \
-s config.evictionHour=[] \
-s config.evictionMinute=[] \
-s config.maxLifespan=[] \
-s 'config.batchSizeForSync=["1000"]' \
-s 'config.editMode=["READ_ONLY"]' \
-s 'config.syncRegistrations=["false"]' \
-s 'config.vendor=["ad"]' \
-s 'config.usernameLDAPAttribute=["sAMAccountName"]' \
-s 'config.rdnLDAPAttribute=["cn"]' \
-s 'config.uuidLDAPAttribute=["objectGUID"]' \
-s 'config.userObjectClasses=["person, organizationalPerson, user"]' \
-s 'config.connectionUrl=["'"${ldapConnectionUrl}"'"]' \
-s 'config.usersDn=["OU=Users,OU=MNIT,DC=COMMERCE-NT1,DC=com"]' \
-s 'config.authType=["simple"]' \
-s 'config.bindDn=["CN=keycloak_ADM,OU=Users,OU=Network Operations,DC=COMMERCE-NT1,DC=com"]' \
-s 'config.bindCredential=["'"${ldapConnectionPassword}"'"]' \
-s 'config.customUserSearchFilter=[]' \
-s 'config.searchScope=["1"]' \
-s 'config.validatePasswordPolicy=["false"]' \
-s 'config.useTruststoreSpi=["ldapsOnly"]' \
-s 'config.connectionPooling=["true"]' \
-s 'config.connectionTimeout=["30000"]' \
-s 'config.readTimeout=["30000"]' \
-s 'config.pagination=["true"]' \
-s 'config.allowKerberosAuthentication=["false"]' \
-s 'config.serverPrincipal=[]' \
-s 'config.keyTab=[""]' \
-s 'config.kerberosRealm=[""]' \
-s 'config.debug=["false"]' \
-s 'config.useKerberosForPasswordAuthentication=["false"]' \
