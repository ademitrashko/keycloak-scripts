#!venv/bin/python
from enum import Enum, auto
from getpass import getpass

import requests

server_url = input('Keycloak URL (http://localhost:8080/auth): ') or 'http://localhost:8080/auth'
username = input('Keycloak admin username: ')
password = getpass('Keycloak admin password: ')

access_token = requests.post(f'{server_url}/realms/master/protocol/openid-connect/token', data={
    'client_id': 'admin-cli',
    'username': username,
    'password': password,
    'grant_type': 'password'
}).json()['access_token']

req_s = requests.Session()
req_s.headers.update({'Authorization': f'Bearer {access_token}'})

realm_location = req_s.post(f'{server_url}/admin/realms', json={'enabled': True, 'realm': 'my_realm'}).headers['Location']

client_url = input('Client URL (http://localhost:9080): ') or 'http://localhost:9080'
client_name = 'my_client'

client_location = req_s.post(f'{realm_location}/clients', json={
    'enabled': True,
    'clientId': client_name,
    'rootUrl': client_url,
    'protocol': 'openid-connect'
}).headers['Location']


# client roles
class ClientRole(Enum):
    MY_ROLE = auto()


for client_role in ClientRole:
    req_s.post(f'{client_location}/roles', json={'name': client_role.name})


# groups
groups = [{
    'name': 'Administrator',
    'roles': [ClientRole.MY_ROLE.name]
}]

client_id = client_location[client_location.rindex('/') + 1:]
client_roles = req_s.get(f'{realm_location}/clients/{client_id}/roles').json()

for group in groups:
    group_location = req_s.post(f'{realm_location}/groups', json={'name': group['name']}).headers['Location']
    group_roles = list(filter(lambda role: role['name'] in group['roles'], client_roles))
    req_s.post(f'{group_location}/role-mappings/clients/{client_id}', json=group_roles)


# confidential client with service accounts
client = req_s.get(client_location).json()
client.update({'publicClient': False, 'serviceAccountsEnabled': True})
req_s.put(client_location, json=client)

service_account_id = req_s.get(f'{realm_location}/users', params={'username': f'service-account-{client_name}'}).json()[0]['id']


# https://bugzilla.redhat.com/show_bug.cgi?id=1730227
req_s.put(f'{realm_location}/users/{service_account_id}', json={'email': f'service-account-{client_name}@state.mn.us'})


# service account roles
realm_clients = req_s.get(f'{realm_location}/clients').json()
realm_management_client_id = next(client for client in realm_clients if client['clientId'] == 'realm-management')['id']

realm_management_client_roles = req_s.get(f'{realm_location}/clients/{realm_management_client_id}/roles').json()

service_account_roles = list(filter(lambda role: role['name'] in ['view-users', 'manage-users'], realm_management_client_roles))

req_s.post(f'{realm_location}/users/{service_account_id}/role-mappings/clients/{realm_management_client_id}', json=service_account_roles)
